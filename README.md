exemple d'appel: https://<service>/api/v1/call?url=https://www.codelutin.com&call=document.title

Permet d'injecter des scripts dans le navigateur
on donne une url (url) et une methode a appeler (call) en query param
cela retourne en body le resultat de l'appel
il est possible de forcer le type mime de retour (mime)

variable d'environnement:
- SAAS_BROWSER_ADDRESS l'adresse et port d'écoute (ex ":40100")
- SAAS_BROWSER_SCRIPT du javascript a interpreter (ex: "function toto() {return 42}")
- SAAS_BROWSER_SCRIPT_FILE liste de fichier javascript (ex: "toto.js,titi.js,tutu.js")
- SAAS_BROWSER_DEFAULT_CALL la methode a appeler (ex: "toto()")
- SAAS_BROWSER_DEFAULT_MIME le type mime a retourner (ex: "text/plain")
