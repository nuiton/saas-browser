FROM golang:alpine AS gobuilder
RUN mkdir /build
WORKDIR /build
COPY . .
RUN GOPROXY=https://proxy.golang.org CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags="-s -w" -o /saas-browser saas-browser.go

FROM chromedp/headless-shell:latest

RUN export DEBIAN_FRONTEND=noninteractive \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    dumb-init \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY --from=gobuilder /saas-browser /usr/local/bin

EXPOSE 40100

ENTRYPOINT ["dumb-init", "--"]
CMD ["/usr/local/bin/saas-browser"]
