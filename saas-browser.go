package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/chromedp"
)

// exemple d'appel: https://<service>/api/v1/call?url=https://www.codelutin.com&call=document.title

// Permet d'injecter des scripts dans le navigateur
// on donne une url (url) et une methode a appeler (call) en query param
// cela retourne en body le resultat de l'appel
// il est possible de forcer le type mime de retour (mime)

// SAAS_BROWSER_ADDRESS l'adresse et port d'écoute (ex ":40100")
// SAAS_BROWSER_SCRIPT du javascript a interpreter (ex: "function toto() {return 42}")
// SAAS_BROWSER_SCRIPT_FILE liste de fichier javascript (ex: "toto.js,titi.js,tutu.js")
// SAAS_BROWSER_DEFAULT_CALL la methode a appeler (ex: "toto()")
// SAAS_BROWSER_DEFAULT_MIME le type mime a retourner (ex: "text/plain")

var proxy = os.Getenv("SAAS_BROWSER_PROXY")
var chromePath = os.Getenv("SAAS_BROWSER_CHROME_PATH")
var callDelay, _ = strconv.Atoi(defaultString(os.Getenv("SAAS_BROWSER_CALL_DELAY"), "0"))

func main() {
	defaultCall := os.Getenv("SAAS_BROWSER_DEFAULT_CALL") // si pas de call dans la demande
	defaultMime := os.Getenv("SAAS_BROWSER_DEFAULT_MIME") // si pas de mime type dans la demande

	scriptFiles := os.Getenv("SAAS_BROWSER_SCRIPT_FILE")
	script := os.Getenv("SAAS_BROWSER_SCRIPT")

	scripts := ""

	// lire les fichiers js
	// concatene les fichiers dans scripts
	for _, file := range strings.Split(scriptFiles, ",") {
		scripts = scripts + readAll(file) + ";"
	}

	// ajout de script inline
	scripts = scripts + script + ";"

	http.HandleFunc("/api/v1/call", func(res http.ResponseWriter, req *http.Request) {
		uri := req.FormValue("url")
		call := defaultString(req.FormValue("call"), defaultCall)
		mime := defaultString(req.FormValue("mime"), defaultMime)

		if checkParam(res, "url", uri) && checkParam(res, "call", call) && checkParam(res, "mime", mime) {
			result, err := evaluate(uri, scripts+call)
			if err != nil {
				http.Error(res, fmt.Sprintf("Can't evaluation script with call '%s' for url '%s': %v", call, uri, err), 500)
			} else {
				res.Header().Set("content-type", mime)
				res.WriteHeader(200)
				res.Write(result)
			}
		}
	})

	addr := defaultString(os.Getenv("SAAS_BROWSER_ADDRESS"), ":40100")
	log.Println("Start web server", addr)
	http.ListenAndServe(addr, nil)
}

func checkParam(res http.ResponseWriter, name string, value string) bool {
	if value == "" {
		http.Error(res, fmt.Sprintf("Parameter '%s' must not be empty", name), 400)
	}

	return true
}

func defaultString(s ...string) string {
	for _, v := range s {
		if v != "" {
			return v
		}
	}

	return ""
}

func readAll(file string) string {
	content, err := ioutil.ReadFile(strings.Trim(file, " "))
	if err != nil {
		log.Printf("file not found '%s': '%v'", file, err)
	}
	return string(content)
}

func evaluate(uri string, script string) ([]byte, error) {
	// setup chromedp default options
	options := []chromedp.ExecAllocatorOption{}
	options = append(options, chromedp.DefaultExecAllocatorOptions[:]...)
	options = append(options, chromedp.DisableGPU)
	options = append(options, chromedp.Flag("ignore-certificate-errors", true))

	if chromePath != "" {
		options = append(options, chromedp.ExecPath(chromePath))
	}

	if proxy != "" {
		options = append(options, chromedp.ProxyServer(proxy))
	}

	actx, acancel := chromedp.NewExecAllocator(context.Background(), options...)
	ctx, cancel := chromedp.NewContext(actx)
	defer acancel()
	defer cancel()

	// run task list
	var result []byte
	err := chromedp.Run(ctx,
		chromedp.Navigate(uri),
		chromedp.Sleep(time.Duration(callDelay)*time.Second),
		chromedp.Evaluate(script, &result, func(p *runtime.EvaluateParams) *runtime.EvaluateParams {
			return p.WithAwaitPromise(true)
		}),
	)

	if err != nil {
		return nil, err
	}

	return result, nil
}
